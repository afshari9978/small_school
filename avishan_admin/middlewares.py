import datetime

from django.core.handlers.wsgi import WSGIRequest

from avishan import current_request


class AvishanAdminContext:
    def __init__(self):
        self.current_request = current_request


class ContextWrapper:

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request: WSGIRequest):
        current_request['context'] = AvishanAdminContext()

        response = self.get_response(request)

        del current_request['context']

        return response
