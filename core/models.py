from django.db import models

# Create your models here.
from avishan.models import AvishanModel, BaseUser


class User(AvishanModel):
    base_user = models.ForeignKey(BaseUser, on_delete=models.CASCADE, related_name='user')
    first_name = models.CharField(max_length=255, blank=True)
    last_name = models.CharField(max_length=255, blank=True)


class Student(AvishanModel):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='student')
    student_id = models.IntegerField()


class Course(AvishanModel):
    title = models.CharField(max_length=255)
    unit = models.IntegerField(default=None, choices=[None, 1, 2, 3, 4], blank=True, null=True)


class StudentCourse(AvishanModel):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='courses')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='students')
    date_created = models.DateTimeField(auto_now_add=True)
